const _ = require('lodash');

module.exports = collection => {
  const aRandomStudent = _.sample(collection);

  return {
    name: aRandomStudent.name,
    HP: _.random(-999, 0),
    DPS: _.random(0, 999),
  };
}