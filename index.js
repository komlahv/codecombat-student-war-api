const express = require('express');
const socket = require('socket.io');
const cors = require('cors')
const fetch = require('isomorphic-fetch');
const getRandomStudent = require('./shared/getRandomStudent');

// App setup
const app = express();
app.use(cors());

let listData;

let port = process.env.PORT || 4000

let server;
let io;

// Static files
// app.use(express.static('public'));

// Socket setup & pass server
const gameData = {
    winner: '',
    players: [],
};

async function startWebSocket() {
    io = socket(server);
    io.on('connection', (socket) => {
        console.log('a user connected');

        //Add user
        gameData.players.push({
            id: socket.id,
            ...getRandomStudent(listData)
        });
        console.log(gameData.players);
        console.log('made socket connection', socket.id);

        //Start game is two players
        function checkPlayers() {
            if (Object.keys(gameData.players).length >= 2) {
                console.log('starting game');
                io.sockets.emit('startGame', gameData);
                return;
            }
            io.sockets.emit('stopGame');
            console.log('waiting for other player');
        }

        checkPlayers();

        socket.on('randomize', function () {
            console.log('randomizing player character', socket.id);
            let userIndex = gameData.players.findIndex((player) => player.id == socket.id);
            gameData.players[userIndex] = { ...gameData.players[userIndex], ...getRandomStudent(listData) };
            io.sockets.emit('startGame', gameData);
        });

        //Handle new data
        socket.on('compete', function () {
            // console.log(data);
            let student1Power = gameData.players[1].HP / gameData.players[0].DPS;
            let student2Power = gameData.players[0].HP / gameData.players[1].DPS;
            student1Power > student2Power ? gameData.winner = gameData.players[0] : gameData.winner = gameData.players[1];

            io.sockets.emit('result', gameData.winner);
        });

        socket.on('restart', function () {
            console.log('restarting game');
            gameData.players[0] = { ...gameData.players[0], ...getRandomStudent(listData) };
            gameData.players[1] = { ...gameData.players[1], ...getRandomStudent(listData) }
            gameData.winner = '';
            io.sockets.emit('startGame', gameData);
        });

        // Handle typing event
        // socket.on('typing', function (data) {
        //     socket.broadcast.emit('typing', data);
        // });

        //Handle disconnect
        socket.on('disconnect', function () {
            console.log('user ' + socket.id + ' disconnected');
            // remove saved socket from users object
            // delete users[socket.id];
            let userIndex = gameData.players.findIndex((player) => player.id == socket.id);
            gameData.players.splice(userIndex, 1)

            //reset winner
            gameData.winner = '';
            io.sockets.emit('result', gameData.winner);

            console.log(gameData.players);
            checkPlayers();
        });
    });
}

async function startServer() {
    //get random people
    let data = await fetch('https://jsonplaceholder.typicode.com/users');
    listData = await data.json();

    server = app.listen(port, async function () {
        console.log(`listening for requests on port ${port}, go to http://localhost:${port}/`);
        startWebSocket();
    });
}

startServer();
